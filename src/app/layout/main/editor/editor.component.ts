import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/shared/services/main-data/data.service';

@Component({
  selector: "app-editor",
  templateUrl: "./editor.component.html",
  styleUrls: ["./editor.component.css"]
})
export class EditorComponent implements OnInit, OnDestroy {
  private subscr: Subscription;
  public dataSource: any[] = [];
  public fc: FormControl;
  public isError = false;

  constructor(private fb: FormBuilder, private dataService: DataService) {}

  ngOnInit() {
    this.fc = this.fb.control("");

    this.subscr = this.dataService.dataObserve$.subscribe(dataSource => {
      this.dataSource = [...dataSource];
    });
  }

  public onSubmit() {
    this.isError = false;
    const value = this.fc.value;

    if (!value) {
      return;
    }

    /* Check validation of custom JSON. JSON keys must have quotes */
    new Promise(resolve => {
      resolve(JSON.parse(value));
    })
      .catch(err => {
        return this.dataService.parseJsonCustom(value);
      })
      .then((dataSourceNew: any[]) => {
        dataSourceNew = this.dataService.setUiid(dataSourceNew);
        this.dataSource = [...this.dataSource, ...dataSourceNew];

        this.fc.reset();
        this.dataService.setData(this.dataSource);
      })
      .catch(err => {
        this.isError = !this.isError;
      });
  }

  public ngOnDestroy() {
    this.subscr.unsubscribe();
  }
}
