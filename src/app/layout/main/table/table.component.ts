import { AfterViewChecked, Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/shared/services';

@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.css"]
})
export class TableComponent implements OnInit, AfterViewChecked, OnDestroy {
  public tableData: any[] = [];
  private subscriptions = new Subscription();
  public displayedColumns: any[];
  private objectUrl: string;
  private tableDataNew: any[];

  constructor(private dataService: DataService, private router: Router) {}

  public ngOnInit(): void {
    this.subscriptions.add(
      this.dataService.dataObserve$.subscribe(data => {
        this.tableData = [...data];
        this.tableDataNew = this.tableData.map(obj => {
         return Object.assign({}, obj);
        })

        if (data.length > 0) {
          this.displayedColumns = Object.keys(this.tableData[0]);
          this.displayedColumns = this.displayedColumns.filter(
            key => key !== "id"
          );
        }
      })
    );
  }

  public ngAfterViewChecked() {

    if (this.tableDataNew.length > 0) {
      this.tableDataNew = this.tableDataNew.map(obj => {
        delete obj.id;
        return obj;
      });
    }

    const blob = new Blob([JSON.stringify(this.tableDataNew)], {
      type: "text/plain"
    });
    const link = document.querySelector("a");

    if (!link) {
      return;
    }

    if (this.objectUrl) {
      URL.revokeObjectURL(this.objectUrl);
    }

    this.objectUrl = URL.createObjectURL(blob);

    link.href = URL.createObjectURL(blob);
    link.download = "test-data.txt";
  }

  public onEdit(id: string) {
    this.router.navigate([`/edit/${id}`]);
  }

  public onDelete(id: string) {
    this.tableData = this.tableData.filter(item => item.id !== id);
    this.dataService.setData(this.tableData);
  }

  public onClear() {
    this.dataService.setData([]);
  }

  public ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
