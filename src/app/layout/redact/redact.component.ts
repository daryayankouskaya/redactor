import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import { DataService } from 'src/app/shared/services';

@Component({
  selector: "app-redact",
  templateUrl: "./redact.component.html",
  styleUrls: ["./redact.component.css"]
})
export class RedactComponent implements OnInit, OnDestroy {
  public displayedColumns: string[];
  public itemForm: FormGroup;
  private id: string;
  private dataSource: any[];

  private subscr = new Subscription();

  constructor(
    private fb: FormBuilder,
    private dataService: DataService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  public ngOnInit() {
    this.subscr.add(
      combineLatest(this.route.params, this.dataService.dataObserve$).subscribe(
        ([params, dataSource]) => {
          if (!params.id || !dataSource) {
            return;
          }

          this.id = params.id;
          this.dataSource = [...dataSource];

          const dataItem = this.dataSource.filter(
            item => item.id === this.id
          )[0];

          if (!dataItem) {
            return;
          }

          const formValue = {};

          this.displayedColumns = Object.keys(dataItem);
          this.displayedColumns = this.displayedColumns.filter(
            key => key !== "id"
          );
          this.displayedColumns.forEach(column => {
            formValue[column] = dataItem[column];
          });
          this.itemForm = this.fb.group(formValue);
        }
      )
    );
  }

  public onSubmit() {
    this.dataSource = this.dataSource.map(item => {
      if (item.id === this.id) {
        return { ...item, ...this.itemForm.value };
      } else {
        return item;
      }
    });
    this.dataService.setData(this.dataSource);
    this.router.navigate(["/main"]);
  }

  public ngOnDestroy() {
    this.subscr.unsubscribe();
  }
}
