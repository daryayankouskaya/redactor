import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import * as uuid4 from 'uuid/v4';

@Injectable({
  providedIn: "root"
})
export class DataService {
  private textData$ = new BehaviorSubject<any[]>([]);

  public get data(): any[] {
    return this.textData$.value;
  }

  public get dataObserve$(): Observable<any[]> {
    return this.textData$.asObservable();
  }

  public setData(value: any[]) {
    this.textData$.next(value);
  }

  public parseJsonCustom(value: string): any[] {
    value = value.replace(/\w*:/gm, match => {
      let tmp = match.slice(0, -1);

      tmp = tmp.trim();

      return `"${tmp}":`;
    });

    return JSON.parse(value);
  }

  public setUiid(value: any[]): any[] {
    return value.map(item => ({ ...item, id: uuid4() }));
  }
}
