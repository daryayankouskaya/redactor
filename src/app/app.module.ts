import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { firebaseConfig } from 'src/environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { LayoutComponent } from './layout/layout.component';
import { EditorComponent } from './layout/main/editor/editor.component';
import { MainComponent } from './layout/main/main.component';
import { TableComponent } from './layout/main/table/table.component';
import { RedactComponent } from './layout/redact/redact.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    EditorComponent,
    RedactComponent,
    TableComponent,
    HeaderComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireStorageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
