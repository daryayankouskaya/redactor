// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyC4es52eEgCTQZYC2mds5Tkellkm29OcSA",
  authDomain: "gallery-app-f68b3.firebaseapp.com",
  databaseURL: "https://gallery-app-f68b3.firebaseio.com",
  projectId: "gallery-app-f68b3",
  storageBucket: "gallery-app-f68b3.appspot.com",
  messagingSenderId: "722612305787",
  appId: "1:722612305787:web:4b2befb4c4bb472318c7de",
  measurementId: "G-BTYNJTFKSS"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
