export const environment = {
  production: true
};

export const firebaseConfig = {
  apiKey: "AIzaSyC4es52eEgCTQZYC2mds5Tkellkm29OcSA",
  authDomain: "gallery-app-f68b3.firebaseapp.com",
  databaseURL: "https://gallery-app-f68b3.firebaseio.com",
  projectId: "gallery-app-f68b3",
  storageBucket: "gallery-app-f68b3.appspot.com",
  messagingSenderId: "722612305787",
  appId: "1:722612305787:web:4b2befb4c4bb472318c7de",
  measurementId: "G-BTYNJTFKSS"
};
